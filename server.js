const express = require('express');
const proxy = require('express-http-proxy');
const path = require('path');
const app = express();
const apiUrl = `${process.env.API_HOST || 'localhost'}:${process.env.API_PORT || 9999}`

app.use(express.static(__dirname + '/ui/dist/'));
app.use('/api', proxy((req, res) => {
    console.log(`redirecting ${req.method} ${req.originalUrl}`);
    return apiUrl;
}));
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'ui/dist/index.html'));
});

app.listen(process.env.PORT || 3000, function () {
    console.log(`Listening on port ${process.env.PORT || 3000}!`);
    console.log(`All /api/* will be redirected to ${apiUrl}/*`)
});
