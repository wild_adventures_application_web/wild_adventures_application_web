const PROXY_CONFIG = {
  "/api": {
    "target": `http://${process.env.API_HOST || "localhost"}:${process.env.API_PORT || 9999}`,
    "secure": false,
    "pathRewrite": {
      "^/api": ""
    }
  }
}

module.exports = PROXY_CONFIG;
