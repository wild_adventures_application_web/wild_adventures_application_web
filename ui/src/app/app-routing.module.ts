import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './shared/layout/main/main.component';
import { LoginComponent } from './modules/authentication/login/login.component';
import { RegisterComponent } from './modules/authentication/register/register.component';
import { CategoryComponent } from './modules/adventures/category/category.component';
import { AdventureComponent } from './modules/adventures/adventure/adventure.component';
import { AdventuresComponent } from './modules/adventures/adventures/adventures.component';
import { LayoutComponent } from './shared/layout/layout/layout.component';
import { LayoutdashbordComponent } from './shared/layout/layoutdashbord/layoutdashbord.component';
import { ProfileComponent } from './modules/dashbord/views/profile/profile.component';
import { OrderListUserComponent } from './modules/dashbord/views/order-list-user/order-list-user.component';
import { MainComponent as PaymentComponent } from './modules/payment/main/main.component';
import { OrderResolverService } from './core/resolvers/order-resolver.service';
import { AuthGuard } from './core/guards/auth.guard';

const routes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    children: [
      {
        path: '',
        component: MainComponent,
      },
      {
        path: 'categories',
        component: CategoryComponent
      },
      {
        path: 'categories/:catId',
        component: AdventuresComponent
      },
      {
        path: 'categories/:catId/adventures/:advId',
        component: AdventureComponent
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'payment',
    component: PaymentComponent,
    resolve: {
      order: OrderResolverService
    },
    canActivate: [AuthGuard]
  },
  {
    path: 'espaceclient',
    component: LayoutdashbordComponent,
    canActivate: [AuthGuard],
    children: [
      { path: '', redirectTo: 'profile', pathMatch: 'full' },
      {
        path: 'profile',
        component: ProfileComponent,
      },
      {
        path: 'orderlistuser',
        component: OrderListUserComponent
      }
    ]
  },
  {
    path: '**',
    redirectTo: '',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
