import { NgModule, Optional, SkipSelf } from '@angular/core';
import { AddressService } from './services/address/address.service';
import { AdventureService } from './services/adventure/adventure.service';
import { AdventureeventService } from './services/adventureevent/adventureevent.service';
import { AlertService } from './services/alert/alert.service';
import { AuthService } from './services/authentication/auth.service';
import { CategoryService } from './services/category/category.service';
import { CommentsService } from './services/comments/comments.service';
import { OrderService } from './services/order/order.service';
import { UsersService } from './services/users/users.service';
import { AuthGuard } from './guards/auth.guard';
import { JwtInterceptor } from './interceptors/jwt.interceptor';
import { ErrorInterceptor } from './interceptors/error.interceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { PaymentService } from './services/payment/payment.service';

@NgModule({
  providers: [
    AddressService,
    AdventureService,
    AdventureeventService,
    AlertService,
    AuthService,
    CategoryService,
    CommentsService,
    OrderService,
    UsersService,
    AuthGuard,
    PaymentService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: JwtInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: ErrorInterceptor,
      multi: true
    },
  ]
})
export class CoreModule {
  constructor(
    @Optional() @SkipSelf() core: CoreModule
  ) {
    if (core) {
      throw new Error('You should import core module only in the root module');
    }
  }
}
