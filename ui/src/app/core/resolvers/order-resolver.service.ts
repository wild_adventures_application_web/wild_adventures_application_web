import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { tap, take, mergeMap } from 'rxjs/operators';
import { Order } from '../models/Order';
import { OrderService } from '../services/order/order.service';

@Injectable({
  providedIn: 'root',
})
export class OrderResolverService implements Resolve<Order> {
  constructor(private orderService: OrderService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Order> | Observable<never> {

    return this.orderService.getSelectedOrder().pipe(
      take(1),
      mergeMap(order => {
        if (order && !order.paymentDate) {
          return of(order);
        } else {
          this.router.navigate(['/espaceclient/orderlistuser']);
          return EMPTY;
        }
      })
    );
  }
}
