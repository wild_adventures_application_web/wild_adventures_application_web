import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { Comment } from '../../models/Comments';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CommentsService {

  baseUrl = '/comment-microservice/v1/comments/';

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/comment-microservice/v1/comments`;
  }
  getMany(itemId: number): Observable<Comment[]> {
    return this.http.get<Comment[]>(`${this.baseUrl}?itemId=${itemId}&filter=true`);
  }

  postOne(comment: Comment): Observable<void> {
    return this.http.post<void>(this.baseUrl, comment);
  }
}
