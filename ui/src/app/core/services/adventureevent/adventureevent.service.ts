import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AdventureEvent } from '../../models/AdventureEvent';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AdventureeventService {

  private baseUrl = '/adventure-microservice/v1/adventure-events/';

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/adventure-microservice/v1/adventure-events`;
  }

  getOne(id: number): Observable<AdventureEvent> {
    return this.http.get<AdventureEvent>(`${this.baseUrl}/${id}`);
  }

  getMany(parentId: number): Observable<AdventureEvent[]> {
    return this.http.get<AdventureEvent[]>(`${this.baseUrl}?parentId=${parentId}&filter=true`);
  }
}
