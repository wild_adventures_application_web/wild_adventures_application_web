import { TestBed } from '@angular/core/testing';

import { AdventureeventService } from './adventureevent.service';

describe('AdventureeventService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AdventureeventService = TestBed.get(AdventureeventService);
    expect(service).toBeTruthy();
  });
});
