import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { Observable } from "rxjs";
import { Adventure } from "../../models/Adventure";
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AdventureService {

  private baseUrl;

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/adventure-microservice/v1/adventures`;
  }

  getOne(id: number): Observable<Adventure> {
    return this.http.get<Adventure>(`${this.baseUrl}/${id}`);
  }

  getMany(parentId: number): Observable<Adventure[]> {
    return this.http.get<Adventure[]>(`${this.baseUrl}?parentId=${parentId}&filter=true`);
  }
}
