import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Order } from '../../models/Order';
import { Observable, BehaviorSubject } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  orderSource = new BehaviorSubject<Order>(undefined);
  selectedOrder = this.orderSource.asObservable();
  baseUrl;

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/order-microservice/v1/orders`;
  }

  setCurrentOrder(order: Order) {
    this.orderSource.next(order);
  }

  getSelectedOrder(): Observable<Order> {
    return this.selectedOrder;
  }

  postOne(order: Order) {
    return this.http.post(this.baseUrl, order);
  }

  getByUser(userId: number): Observable<Order[]> {
    return this.http.get<Order[]>(`${this.baseUrl}?filter=true&userId=${userId}`);
  }

  getAll() {
    return this.http.get(this.baseUrl);
  }

}
