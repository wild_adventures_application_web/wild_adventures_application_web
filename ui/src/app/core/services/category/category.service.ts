import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpParams } from "@angular/common/http";
import { User } from "../../models/User";
import { Observable } from "rxjs";
import { Category } from "../../models/Category";
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  categories: Category[];
  private baseUrl;

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/adventure-microservice/v1/categories`;
  }

  getOne(id: number) {
    return this.http.get<Category>(`${this.baseUrl}/${id}`);
  }
  getMany(): Observable<Category[]> {

    return this.http.get<Category[]>(this.baseUrl);
  }
  //   postMany() {
  //     return this.http.post(`localhost:8080/v1/category/`);
  //   }
  //   putMany() {
  //   return this.http.put(`localhost:8080/v1/category/`);
  // }
  //   deleteMany() {
  //   return this.http.delete(`localhost:8080/v1/category/`);
}

