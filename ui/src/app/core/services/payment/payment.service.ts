import { Injectable, Inject } from '@angular/core';
import { PaymentDetail } from '../../models/payment';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class PaymentService {

  private baseUrl = '/payment-microservice/v1/payments/';

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/payment-microservice/v1/payments`;
  }

  postPayment(paymentDetail: PaymentDetail) {
    return this.http.post(this.baseUrl, paymentDetail);
  }
}
