import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../models/User';
import * as jwt_decode from 'jwt-decode';
import { Observable } from 'rxjs';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private baseUrl = '/payment-microservice/v1/payments/';

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/microservice-users/v1/users`;
  }

  getCurrent(): Observable<User> {
    const token = localStorage.getItem('currentUser');
    const decoded = jwt_decode(token);
    return this.getCompletByMail(decoded['sub']);
  }

  getById(id: number): Observable<User> {
    return this.http.get<User>(`${this.baseUrl}/id/${id}`);
  }

  getByMail(mail: string) {
    return this.http.get(`${this.baseUrl}/mail/${mail}`);
  }
  getCompletByMail(mail: string) {
    return this.http.get<User>(`${this.baseUrl}/mailComplet/${mail}`);
  }

  register(user: User) {
    return this.http.post(this.baseUrl, user);
  }

  update(user: User, id: number) {
    return this.http.patch(`${this.baseUrl}/id/${id}`, user);
  }
}
