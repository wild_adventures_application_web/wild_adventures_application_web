import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Address } from "../../models/Address";
import { Observable } from "rxjs";
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  private baseUrl;

  constructor(private http: HttpClient, @Inject(DOCUMENT) private document) {
    this.baseUrl = `${document.location.origin}/api/microservice-users/address`;
  }

  register(address: Address) {
    return this.http.post(this.baseUrl, address);
  }

  getAddressById(id: number): Observable<Address[]> {
    return this.http.get<Address[]>(`${this.baseUrl}/id/${id}`);
  }
}
