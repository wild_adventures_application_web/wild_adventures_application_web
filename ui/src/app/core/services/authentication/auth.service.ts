import { Injectable, Inject } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from '../../models/User';
import { DOCUMENT } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  tokenUser: string;
  private baseUrl;

  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  constructor(
    private http: HttpClient,
    @Inject(DOCUMENT) private document
  ) {
    this.baseUrl = `${document.location.origin}/api`;
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(): User {
    return this.currentUserSubject.value;
  }

  login(username: string, password: string) {
    return this.http.post<any>(`${this.baseUrl}/auth`, { username, password }, { observe: 'response'})
      .pipe(map(response => {
        // login successful if there's a jwt token in the response
        if (response && response.headers.get('Authorization').substring(7)) {
          // store user details and jwt token in local storage to keep user logged in between page refreshes
          this.tokenUser = response.headers.get('Authorization').substring(7);
          localStorage.setItem('currentUser', JSON.stringify(this.tokenUser));
          this.currentUserSubject.next(this.tokenUser);
        }

        return this.tokenUser;
      }));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }
}
