export class Address {
  id_address: number;
  id_user: number;
  id_location: number;
  name: string;
  name_address: string;
  city: string;
  country: string;
  postalcode: string;
  door: string;
  stage: string;
  portalCode: string;
}
