export interface PaymentDetail {
  payment: Payment;
  orders: SimpleOrder[];
}

export interface SimpleOrder {
  id: number;
}

export class Payment {
  card: Card = new Card();
  billingDetails: BillingDetails = new BillingDetails();
}

export class BillingDetails {
  street: string;
  city: string;
  state: string;
  country: string;
  zip: string;
}

export class Card {
  cardNum: string;
  cardExpiry: CardExpiry = new CardExpiry();
}

export class CardExpiry {
  month: number;
  year: number;
}
