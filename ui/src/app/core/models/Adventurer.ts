import { AdventureEvent } from "./AdventureEvent";

export class Adventurer {
  id: number;
  adventureEvents: AdventureEvent[];
}
