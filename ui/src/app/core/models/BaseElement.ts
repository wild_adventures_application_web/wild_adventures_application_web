import {Comment} from "./Comments";

export class BaseElement {
  id: number;
  description: string;
  image: string;
  name: string;
  comments: Comment[];
}
