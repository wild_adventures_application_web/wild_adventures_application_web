export class Comment {
  id?: number;
  userId: number;
  itemId: number;
  content: string;
}
