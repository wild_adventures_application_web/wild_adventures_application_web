import { BaseElement } from "./BaseElement";
import { AdventureEvent } from "./AdventureEvent";
import { Category } from "./Category";

export class Adventure extends BaseElement{
  price: number;
  category: Category;
  adventureEvents: AdventureEvent[];
}
