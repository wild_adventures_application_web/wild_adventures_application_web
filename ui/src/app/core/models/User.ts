import { Address } from "./Address";

export class User {
  id_user: number;
  id?: number; // Fetch from adventure service the users have id
  id_profil: number;
  mail: string;
  password: string;
  name: string;
  lastname: string;
  profil: string;
  addressList: Address[];
  token?: string;
}
