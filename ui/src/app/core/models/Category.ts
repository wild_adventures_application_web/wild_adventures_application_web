import { BaseElement } from "./BaseElement";
import { Adventure } from "./Adventure";
import {Comment} from "./Comments";

export class Category extends BaseElement{
   adventures: Adventure[];
 }
