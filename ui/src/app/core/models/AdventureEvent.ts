import { BaseElement } from './BaseElement';
import { Adventure } from './Adventure';
import { User } from './User';

export class AdventureEvent extends BaseElement {
  date: string;
  adventure: Adventure;
  adventurers: User[];
}
