export class Order {
  id?: number;
  adventureEventId: number;
  userId: number;
  orderDate?: string;
  paymentDate?: string;
  price?: number;
}
