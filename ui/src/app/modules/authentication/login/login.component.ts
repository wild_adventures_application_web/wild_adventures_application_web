import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import { AuthService } from '../../../core/services/authentication/auth.service';
import {ActivatedRoute, Router} from '@angular/router';
import { first } from 'rxjs/operators';
import {AlertService} from '../../../core/services/alert/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loading = false;
  submitted = false;
  returnUrl: string;
  formLogin: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private alertService: AlertService
  ) {
    // redirect to home if already logged in
    if (this.authService.currentUserValue) {
      this.router.navigate(['/']);
    }
  }
// convenience getter for easy access to form fields
  get field() {
    return this.formLogin.controls;
  }
  initForm() {
    this.formLogin = this.formBuilder.group({
      formEmail: ['', Validators.required],
      formPassword: ['', Validators.required]
    });
  }
  ngOnInit() {
    this.initForm();
    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }
  login() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.formLogin.invalid) {
      return;
    }
    this.loading = true;
    this.authService.login(this.field.formEmail.value, this.field.formPassword.value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        error => {
          this.alertService.error(error);
          this.loading = false;
        });
  }
}
