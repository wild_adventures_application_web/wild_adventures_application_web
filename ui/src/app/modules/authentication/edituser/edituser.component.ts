import { Component, OnInit } from '@angular/core';
import {User} from "../../../core/models/User";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {UsersService} from "../../../core/services/users/users.service";
import {first} from "rxjs/operators";
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-edituser',
  templateUrl: './edituser.component.html',
  styleUrls: ['./edituser.component.scss']
})
export class EdituserComponent implements OnInit {
  private user: User;
  editForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UsersService
  ) {}

  ngOnInit() {
    this.getUserWithMail();
    this.editForm = this.formBuilder.group({
      id_user: [],
      id_profil: [],
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      mail: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      profil: []
    });

  }

  decodeTokenForMail(){
    let token = localStorage.getItem("currentUser");
    if(!token) {
      alert("Invalid action.")
      this.router.navigate(['/login']);
      return;
    }
    var decoded = jwt_decode(token)
    console.log(decoded['sub']);
    return decoded['sub'];
  }

  private getUserWithMail(): void{
    this.userService.getCompletByMail(this.decodeTokenForMail())
      .pipe(first())
      .subscribe(user => {
        this.user = user
      });
    // console.log(this.user);
  }

  onSubmit() {
    this.editForm.controls['id_user'].setValue(this.user.id_user);
    this.editForm.controls['profil'].setValue(this.user.profil);
    this.editForm.controls['id_profil'].setValue(this.user.id_profil);
    this.userService.update(this.editForm.value, this.user.id_user)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/login']);
        },
        error => {
          alert(error);
        });
  }

}
