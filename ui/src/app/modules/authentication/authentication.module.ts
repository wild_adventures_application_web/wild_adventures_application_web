import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { EdituserComponent } from './edituser/edituser.component';
import {AuthenticationRoutingModules} from "./authentication-routing.modules";

@NgModule({
  declarations: [
    LoginComponent,
    RegisterComponent,
    EdituserComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AuthenticationRoutingModules,
  ],
  exports: [
    LoginComponent
  ]
})
export class AuthenticationModule { }
