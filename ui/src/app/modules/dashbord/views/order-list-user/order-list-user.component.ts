import { Component, OnInit } from '@angular/core';
import { Order } from '../../../../core/models/Order';
import { OrderService } from '../../../../core/services/order/order.service';
import { UsersService } from '../../../../core/services/users/users.service';
import { switchMap } from 'rxjs/operators';
import { Observable, merge } from 'rxjs';
import { AdventureEvent } from '../../../../core/models/AdventureEvent';
import { AdventureeventService } from '../../../../core/services/adventureevent/adventureevent.service';
import { Adventure } from '../../../../core/models/Adventure';
import { Router } from '@angular/router';

@Component({
  selector: 'app-basic-table',
  templateUrl: './order-list-user.component.html',
  styleUrls: ['./order-list-user.component.scss']
})
export class OrderListUserComponent implements OnInit {
  orders: Order[];
  adventureevents = new Map<number, AdventureEvent>();

  constructor(
    private userService: UsersService,
    private orderService: OrderService,
    private adventureeventService: AdventureeventService,
    private router: Router
  ) { }

  ngOnInit() {
    this.userService.getCurrent()
      .pipe(switchMap(user => this.orderService.getByUser(user.id_user)))
      .pipe(switchMap(orders => {
        this.orders = orders;
        const observables: Observable<AdventureEvent>[] = [];

        for (const order of orders) {
          observables.push(this.adventureeventService.getOne(order.adventureEventId));
        }

        return merge(...observables);
      }))
      .subscribe(adventureevent => this.adventureevents.set(adventureevent.id, adventureevent));
  }

  getAssociatedAdventure(order: Order): Adventure {
    const advEvent = this.adventureevents.get(order.adventureEventId);

    return advEvent ? advEvent.adventure : undefined;
  }

  getAssociatedAdventureLink(order: Order): string {
    let link;
    const advEvent = this.adventureevents.get(order.adventureEventId);

    if (advEvent) {
      const adv = advEvent.adventure;
      const cat = adv.category;
      link = `/categories/${cat.id}/adventures/${adv.id}`;
    }

    return link;
  }

  onChoseOrder(order: Order) {
    this.orderService.setCurrentOrder(order);
    this.router.navigate(['/payment']);
  }
}
