import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from '../../../../core/models/User';
import { UsersService } from '../../../../core/services/users/users.service';
import { first } from 'rxjs/operators';
import { Router } from '@angular/router';
import * as jwt_decode from 'jwt-decode';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  name: string;
  lastname: string;
  mail: string;
  user: User;
  profilForm: FormGroup;
  addressForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService: UsersService
  ) { }

  ngOnInit() {
    this.userService.getCurrent().subscribe(user => {
      this.user = user;
    });


    this.profilForm = this.formBuilder.group({
      id_user: new FormControl(),
      id_profil: new FormControl(),
      name: ['', Validators.required],
      lastname: ['', Validators.required],
      mail: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]],
      profil: new FormControl(),
    });

  }

  onSubmit() {
    this.profilForm.controls['id_user'].setValue(this.user.id_user);
    this.profilForm.controls['profil'].setValue(this.user.profil);
    this.profilForm.controls['id_profil'].setValue(this.user.id_profil);
    this.profilForm.controls['mail'].setValue(this.user.mail);
    this.userService.update(this.profilForm.value, this.user.id_user)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['/login']);
        },
        error => {
          alert(error);
        });
  }

  onSubmitAddress() {

  }

}
