import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {AuthService} from "../../../core/services/authentication/auth.service";


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  @ViewChild('sidenav') sidenav: ElementRef;

  clicked: boolean;

  constructor(
    private auth: AuthService,
  ) {
    this.clicked = this.clicked === undefined ? false : true;
  }

  ngOnInit() {
  }

  setClicked(val: boolean): void {
    this.clicked = val;
  }
  isLogged() {
    return localStorage.getItem('currentUser') != null;
  }

}
