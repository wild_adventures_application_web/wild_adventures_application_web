import { Component, Input, OnInit } from '@angular/core';
import { Adventure } from '../../../core/models/Adventure';
import { CommentsService } from '../../../core/services/comments/comments.service';
import { Comment } from '../../../core/models/Comments';
import { Order } from '../../../core/models/Order';
import { AdventureEvent } from '../../../core/models/AdventureEvent';
import { AdventureeventService } from '../../../core/services/adventureevent/adventureevent.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AdventureService } from 'src/app/core/services/adventure/adventure.service';
import { UsersService } from 'src/app/core/services/users/users.service';
import { OrderService } from 'src/app/core/services/order/order.service';
import { switchMap } from 'rxjs/operators';
import { Observable, merge } from 'rxjs';
import { User } from 'src/app/core/models/User';

class CommentUserHolder {
  comment?: Comment;
  user?: User;
  backGround: string;


  constructor(comment?: Comment, user?: User, justAdded?: boolean) {
    this.comment = comment;
    this.user = user;
    this.backGround = justAdded ? 'bg-success p-2' : '';
  }
}

@Component({
  selector: 'app-adventure',
  templateUrl: './adventure.component.html',
  styleUrls: ['./adventure.component.scss']
})
export class AdventureComponent implements OnInit {
  currentAdventure: Adventure;
  currentUser: User;
  commentUserHolders: CommentUserHolder[];
  adventureEvents: AdventureEvent[];
  orders: Order[];
  textComment = '';

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private commentsService: CommentsService,
    private adventureService: AdventureService,
    private adventureEventsService: AdventureeventService,
    private userService: UsersService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    const adventureId = parseInt(this.route.snapshot.paramMap.get('advId'), 10);
    this.adventureService.getOne(adventureId)
      .subscribe(adventure => {
        this.currentAdventure = adventure;
        this.getAdventureEvents();
        this.getComments();
        this.getCurrentUserAndOrders();
      });
  }

  getAdventureEvents(): void {
    this.adventureEventsService.getMany(this.currentAdventure.id)
      .subscribe(adventureEvents => {
        this.adventureEvents = adventureEvents;
      });
  }

  getComments(): void {
    this.commentsService.getMany(this.currentAdventure.id)
      .pipe(switchMap(comments => {
        this.commentUserHolders = comments.map(comment => new CommentUserHolder(comment));
        const observables: Observable<User>[] = [];

        for (const comment of comments) {
          observables.push(this.userService.getById(comment.userId));
        }

        return merge(...observables);
      }))
      .subscribe(user => {
        this.commentUserHolders.forEach(comment => {
          if (user.id_user === comment.comment.userId) {
            comment.user = user;
          }
        });
      });
  }

  getCurrentUserAndOrders(): void {
    this.userService.getCurrent()
      .pipe(switchMap(user => {
        this.currentUser = user;
        return this.orderService.getByUser(user.id_user);
      }))
      .subscribe(orders => {
        this.orders = orders;
      });
  }

  order(adventureEvent: AdventureEvent): void {
    const order = {
      adventureEventId: adventureEvent.id,
      userId: this.currentUser.id_user
    };

    this.orderService.postOne(order).subscribe(_ => {
      this.currentUser.id = this.currentUser.id_user;
      adventureEvent.adventurers.push(this.currentUser);
    });
  }

  canBook(adventureEvent: AdventureEvent) {
    const index = adventureEvent.adventurers.findIndex(user => user.id === this.currentUser.id_user);
    return index === -1;
  }

  sendComment() {
    const commentToPost: Comment = {
      userId: this.currentUser.id_user,
      itemId: this.currentAdventure.id,
      content: this.textComment
    };

    this.commentsService.postOne(commentToPost)
      .subscribe(_ => {
        console.log(new CommentUserHolder(undefined, undefined, true))
        this.commentUserHolders.push(new CommentUserHolder(commentToPost, this.currentUser, true));
        this.resetComment();
      });
  }

  resetComment() {
    this.textComment = '';
  }

}
