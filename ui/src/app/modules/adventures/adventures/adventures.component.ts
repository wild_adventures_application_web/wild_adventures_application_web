import { Component, Input, OnInit } from '@angular/core';
import { Category } from '../../../core/models/Category';
import { Adventure } from '../../../core/models/Adventure';
import { AdventureService } from '../../../core/services/adventure/adventure.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-adventures',
  templateUrl: './adventures.component.html',
  styleUrls: ['./adventures.component.scss']
})
export class AdventuresComponent implements OnInit {
  categoryId: number;
  adventures: Adventure[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private adventureService: AdventureService,
  ) { }

  ngOnInit() {
    this.categoryId = parseInt(this.route.snapshot.paramMap.get('catId'), 10);
    this.adventureService.getMany(this.categoryId)
      .subscribe(adventures => {
        this.adventures = adventures;
      });
  }
  onSelect(adventure: Adventure): void {
    this.router.navigate(
      ['adventures', adventure.id],
      {
        relativeTo: this.route,
        state: { adventure }
      }
    );
  }
}
