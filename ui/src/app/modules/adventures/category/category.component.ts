import { Component, OnInit } from '@angular/core';
import { Category } from '../../../core/models/Category';
import { CategoryService } from '../../../core/services/category/category.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  categories: Category[];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private categoriesService: CategoryService,
  ) { }

  ngOnInit() {
    this.getCategories();
  }

  onSelect(category: Category): void {
    this.router.navigate([category.id], { relativeTo: this.route });
  }

  getCategories(): void {
    this.categoriesService.getMany()
      .subscribe(categories => {
        this.categories = categories;
      });
  }
}
