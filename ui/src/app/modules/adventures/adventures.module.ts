import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CategoryComponent } from './category/category.component';
import { AdventureComponent } from './adventure/adventure.component';
import { AdventuresComponent } from './adventures/adventures.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    CategoryComponent,
    AdventureComponent,
    AdventuresComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    CommonModule,
    FormsModule
  ]
})
export class AdventuresModule {

}
