import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main/main.component';
import { FormComponent } from './form/form.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [MainComponent, FormComponent],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [MainComponent]
})
export class PaymentModule { }
