import { Component, OnInit, Input, Output, EventEmitter, LOCALE_ID, Inject } from '@angular/core';
import { Order } from 'src/app/core/models/Order';
import { Payment } from 'src/app/core/models/payment';
import { COUNTRIES } from 'src/app/const';

@Component({
  selector: 'app-payment-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {

  countries = COUNTRIES;

  @Output() paymentFormEvent = new EventEmitter<Payment>();
  paymentForm: Payment = new Payment();

  constructor(@Inject(LOCALE_ID) private locale: string) { }

  ngOnInit() {
    this.paymentForm.billingDetails.country = this.locale.split('-')[1];
  }

  triggerEvent() {
    this.paymentFormEvent.next(this.paymentForm);
  }

}
