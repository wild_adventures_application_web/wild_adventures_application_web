import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Order } from 'src/app/core/models/Order';
import { OrderService } from 'src/app/core/services/order/order.service';
import { switchMap } from 'rxjs/operators';
import { AdventureeventService } from 'src/app/core/services/adventureevent/adventureevent.service';
import { AdventureEvent } from 'src/app/core/models/AdventureEvent';
import { Payment, PaymentDetail } from 'src/app/core/models/payment';
import { PaymentService } from 'src/app/core/services/payment/payment.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-payment-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  selectedOrder: Order;
  adventureEvent: AdventureEvent;
  payment: Payment;

  constructor(
    private activatedRoute: ActivatedRoute,
    private location: Location,
    private adventureEventService: AdventureeventService,
    private paymentService: PaymentService
  ) { }

  ngOnInit() {
    this.activatedRoute.data
      .pipe(switchMap((data: { order: Order }) => {
        this.selectedOrder = data.order;

        return this.adventureEventService.getOne(data.order.adventureEventId);
      }))
      .subscribe(adventureEvent => this.adventureEvent = adventureEvent);
  }

  onPaymentEvent(event: Payment) {
    const paymentDetail: PaymentDetail = {
      payment: event,
      orders: [
        {
          id: this.selectedOrder.id
        }
      ]
    };
    console.log(paymentDetail)
    this.paymentService.postPayment(paymentDetail)
      .subscribe(_ => this.location.back());
  }

}
