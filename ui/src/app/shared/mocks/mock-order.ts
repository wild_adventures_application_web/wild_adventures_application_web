import { Order } from "../../core/models/Order";

export function ORDERS(): Order[] {
  return [
    {
      id: 1,
      adventureEventId: 1,
      userId: 1,
      orderDate: null,
      paymentDate: null,
      price: 500,
    },
    {
      id: 2,
      adventureEventId: 1,
      userId: 1,
      orderDate: null,
      paymentDate: null,
      price: 1000,
    }
  ];
}
