import {Category} from "../../core/models/Category";
import {Adventure} from "../../core/models/Adventure";
import {Comment} from "../../core/models/Comments";

export function CATEGORIES(adventures: Adventure[], adventures2: Adventure[], comments: Comment[]): Category[] {
  return [
    {
      id: 1,
      name: 'Safari',
      description: 'Y a-t-il quelque chose comme être à portée de main d\'un lion pour piquer vos sens et vous faire sentir vivant? Promenez-vous du côté sauvage. Détendez-vous dans l\'Indiana Jones avec un programme de voyages d\'aventure comme vous ne l\'avez jamais fait auparavant. Les voyageurs de safari peuvent s’attendre à de nombreuses curiosités ébouriffantes dans des destinations improbables.',
      image: 'https://www.goabroad.com/section_cloudinary/gaplabs/image/upload/images2/program_content/10-totally-fresh-types-of-adventures-for-your-future-travels-2-1510126473.png',
      adventures: adventures,
      comments: comments
    },
    {
      id: 2,
      name: 'Camps d\'été',
      description: '«Cette fois-ci au camp du groupe» est devenu un cliché pour une raison: parce que le camp d\'été est la source ultime d\'aventures absurdes et merveilleuses - celles avec lesquelles vous pouvez embarrasser vos petits-enfants pendant des décennies. Comptez sur de nombreuses créations avec des matériaux naturels, des randonnées de groupe et des copains dans le cadre de votre programme de voyages d\'aventure dans le camp d\'été. Les chants et s\'mores au feu de camp à la fin de chaque nuit ne sont que la cerise sur le gâteau.',
      image: 'https://www.goabroad.com/section_cloudinary/gaplabs/image/upload/images2/program_content/10-totally-fresh-types-of-adventures-for-your-future-travels-3-1510126522.png',
      adventures: adventures,
      comments: comments
    },
    {
      id: 3,
      name: 'Faire du vélo',
      description: 'Ne réinventons pas la roue. Bien sûr, les vélos existent depuis des lustres (depuis 1817 en Allemagne), mais ce qui fait du cyclisme une activité d\'aventure inédite, c\'est le flot incessant de nouveaux chemins à explorer. Des colportages agréables dans les vallées exotiques au VTT extrême, vous pouvez rendre votre aventure à vélo aussi sauvage ou apprivoisée que vous le souhaitez.',
      image: 'https://www.goabroad.com/section_cloudinary/gaplabs/image/upload/images2/program_content/10-totally-fresh-types-of-adventures-for-your-future-travels-4-1510126541.png',
      adventures: null,
      comments: null
    },
    {
      id: 4,
      name: 'Kayak',
      description: 'L\'eau représente plus de la moitié du corps, il est donc naturel que les voyageurs aspirent à renouer avec leurs racines H20. Si vous avez envie de bras, d’air pur et de rythme dans votre aventure, vous adorerez faire du kayak. Attrapez des vagues fantastiques en kayak de mer, évitez les tourbillons tourbillonnaires en kayak de rivière ou détendez-vous en kayak sur le lac. Quelle que soit la façon dont vous le découpez, le kayak est un plaisir fou.',
      image: 'https://www.goabroad.com/section_cloudinary/gaplabs/image/upload/images2/program_content/10-totally-fresh-types-of-adventures-for-your-future-travels-5-1510126556.png',
      adventures: adventures2,
      comments: null
    },
    {
      id: 5,
      name: 'Escalade',
      description: 'Nous sommes dans le nouveau millénaire et les humains peuvent escalader les falaises maintenant. #ÉPIQUE! L\'escalade est un excellent moyen de rencontrer des gens qui partagent les mêmes idées et de passer une journée glorieuse à l\'extérieur, lors de vos voyages. Allez avec un fournisseur de programme pour vous assurer que vous frappez tous les meilleurs rochers en ville. Et pendant que vos mains hurlent à mi-hauteur, rappelez-vous à quel point la vue au sommet sera incroyable!',
      image: 'https://www.goabroad.com/section_cloudinary/gaplabs/image/upload/images2/program_content/10-totally-fresh-types-of-adventures-for-your-future-travels-8-1510126612.png',
      adventures: null,
      comments: null
    },
    {
      id: 6,
      name: 'Traîneau à chiens',
      description: 'Nous pouvons tous convenir que les chiens sont le meilleur ami de l\'homme. Mais tant que vous ne serez pas tombés dans la neige avec une meute de chiots fringants, vous ne saurez jamais jusqu\'où cette amitié peut aller. Vivez des aventures uniques dans les environnements les plus frais et les plus poudreux: la neige. Ne soyez pas pris au dépourvu en pensant qu\'il sera facile de se faire tirer, le traîneau à chiens est étonnamment exigeant physiquement!',
      image: 'https://www.goabroad.com/section_cloudinary/gaplabs/image/upload/images2/program_content/10-totally-fresh-types-of-adventures-for-your-future-travels-7-1510126596.png',
      adventures: null,
      comments: null
    }
  ];
}
