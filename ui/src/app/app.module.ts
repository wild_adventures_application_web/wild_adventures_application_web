import { BrowserModule } from '@angular/platform-browser';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { LOCALE_ID } from '@angular/core';
import localeFr from '@angular/common/locales/fr';
registerLocaleData(localeFr, 'fr');

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatDialogModule } from '@angular/material/dialog';

import { MDBBootstrapModule } from 'angular-bootstrap-md';

import { AuthenticationModule } from './modules/authentication/authentication.module';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { ErrorInterceptor } from './core/interceptors/error.interceptor';
import { JwtInterceptor } from './core/interceptors/jwt.interceptor';
import { CoreModule } from './core/core.module';
import { HeaderComponent } from './shared/layout/header/header.component';
import { FooterComponent } from './shared/layout/footer/footer.component';
import { AdventuresModule } from './modules/adventures/adventures.module';
import { MainComponent } from './shared/layout/main/main.component';
import { LayoutComponent } from './shared/layout/layout/layout.component';
import { AlertComponent } from './shared/layout/alert/alert.component';
import { LayoutdashbordComponent } from './shared/layout/layoutdashbord/layoutdashbord.component';
import { ViewsModule } from './modules/dashbord/views/views.module';
import { NavigationModule } from './modules/dashbord/navigation/navigation.module';
import { PaymentModule } from './modules/payment/payment.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    MainComponent,
    FooterComponent,
    LayoutComponent,
    AlertComponent,
    LayoutdashbordComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    MatDialogModule,
    AppRoutingModule,
    HttpClientModule,
    CoreModule,
    AuthenticationModule,
    AdventuresModule,
    NavigationModule,
    ViewsModule,
    PaymentModule,
  ],
  schemas: [
    NO_ERRORS_SCHEMA,
    CUSTOM_ELEMENTS_SCHEMA,
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'fr-FR'
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
