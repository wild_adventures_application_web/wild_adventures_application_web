FROM node:8

LABEL name="${appName}"
LABEL version="${version}"

WORKDIR /app

COPY node_modules node_modules
COPY package.json package.json
COPY server.js server.js
COPY ui/dist ui/dist

CMD ["node", "server.js"]